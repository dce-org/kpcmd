module gitlab.com/dce-org/kpcmd

go 1.15

require (
	github.com/mitchellh/go-homedir v1.1.0
	github.com/spf13/cobra v1.1.3
	github.com/spf13/viper v1.7.0
	github.com/tobischo/gokeepasslib/v3 v3.1.0
)
