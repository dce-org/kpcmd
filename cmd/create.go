/*
Copyright © 2021 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"fmt"
	"log"
	"os"

	homedir "github.com/mitchellh/go-homedir"
	"github.com/spf13/cobra"
	"github.com/tobischo/gokeepasslib/v3"
)

// createCmd represents the create command
var (
	dbFile string
	dbName string
	masterPassword string

	createCmd = &cobra.Command{
		Use:   "create",
		Short: "Create a new keepass database",
		Run: func(cmd *cobra.Command, args []string) {
			if dbFile == "" {
				home, err := homedir.Dir()
				if err != nil {
					fmt.Println("Home not found.")
					os.Exit(1)
				}
				dbFile = home + "/.keepass.kdbx"
			}

			if _, err := os.Stat(dbFile); err == nil || os.IsExist(err) {
				fmt.Printf("File %s exists.\n", dbFile)
				os.Exit(1)
			}

			file, err := os.Create(dbFile)
			if err != nil {
				panic(err)
			}
			defer file.Close()

			// create the new database
			db := gokeepasslib.NewDatabase(
				gokeepasslib.WithDatabaseKDBXVersion4(),
			)
			db.Content.Meta.DatabaseName = dbName
			db.Credentials = gokeepasslib.NewPasswordCredentials(masterPassword)

			// Lock entries using stream cipher
			db.LockProtectedEntries()

			// and encode it into the file
			keepassEncoder := gokeepasslib.NewEncoder(file)
			if err := keepassEncoder.Encode(db); err != nil {
				panic(err)
			}

			log.Printf("Wrote kdbx file: %s", dbFile)
		},
	}
)

func init() {
	rootCmd.AddCommand(createCmd)
	createCmd.PersistentFlags().StringVarP(&dbFile, "file", "f", "", "database file (default is $HOME/.keepass.kdbx)")
	createCmd.PersistentFlags().StringVarP(&masterPassword, "password", "p", "", "master password")
	createCmd.MarkPersistentFlagRequired("password")
	createCmd.PersistentFlags().StringVarP(&dbName, "name", "n", "New Database", "database name")
}
